import gitlab
import getpass

data = {
    'branch': 'master',
    'commit_message': 'Init commit',
    'actions': [
        {
            'action': 'create',
            'file_path': 'README.MD',
            'content': open('content/README.MD').read(),
        },
        {
            'action': 'create',
            'file_path': '.gitlab-ci.yml',
            'content': open('content/compiled/.gitlab-ci.yml').read(),
        },
    ]
}

input_private_token = getpass.getpass('Input your private token: ')

with gitlab.Gitlab('https://gitlab.com/', private_token=input_private_token) as gl:
    input_group_id = input('Input group ID or URL: ')

    group = gl.groups.get(input_group_id)
    group_access_requests = group.accessrequests.list()

    for access_request in group_access_requests:
        access_request.delete()
        print(f'Deleted access request from member {access_request.username}')

        new_project = gl.projects.create({'name': f'{group.name} - {access_request.username}',
                                          'namespace_id': group.id})
        print(f'Created project {new_project.name}')

        member = new_project.members.create({'user_id': access_request.id,
                                             'access_level': gitlab.DEVELOPER_ACCESS})
        print('Added member to this project')

        commit = new_project.commits.create(data)
        print('Created init commit')
        print('---------------------')

print('Finished')
